package com.example.andrii_iliunin.aspectlogger;

import com.example.andrii_iliunin.aspectlogger.annotation.EventLog;
import com.example.andrii_iliunin.aspectlogger.factory.AspectLoggerServiceLocator;
import com.example.andrii_iliunin.aspectlogger.logger.AspectLogger;

import java.util.Arrays;

public class Test {

    @EventLog("default")
    private void good(String arg) {

    }

    @EventLog("special")
    private void soGood(String arg1, String arg2) {

    }

    public static void main(String[] args) {
        AspectLoggerServiceLocator.putLogger("default", new AspectLogger<ValuesHolder>() {

            @Override
            public void log(ValuesHolder instance) {
                System.out.println(describe() + ": " + instance.toString());
            }

            @Override
            public ValuesHolder convert(String name, Object[] args) {
                String[] result = new String[args.length];
                for (int i = 0; i < args.length; i++) {
                    result[i] = (String) args[i];
                }
                return new ValuesHolder(result);
            }

            @Override
            public String describe() {
                return "Default channel";
            }
        });

        AspectLoggerServiceLocator.putLogger("special", new AspectLogger<ValuesHolder>() {
            @Override
            public void log(ValuesHolder instance) {
                System.out.println(describe() + ": " + instance.toString());
            }

            @Override
            public ValuesHolder convert(String name, Object[] args) {
                String[] result = new String[args.length];
                for (int i = 0; i < args.length; i++) {
                    result[i] = (String) args[i];
                }
                return new ValuesHolder(result);
            }

            @Override
            public String describe() {
                return "Special channel";
            }
        });

        Test test = new Test();
        test.good("str");
        test.good("strstr");
        test.soGood("str", "str");
        test.soGood("strstr", "strstr");

    }

    private static class ValuesHolder {

        private String[] strings;

        ValuesHolder(String[] strings) {
            this.strings = strings;
        }

        private void setStrings(String... args) {
            strings = args;
        }

        @Override
        public String toString() {
            return Arrays.toString(strings);
        }

    }
}
