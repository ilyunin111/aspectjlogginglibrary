package com.example.andrii_iliunin.aspectlogger.example_with_custom_class_to_log.entity;

import java.util.HashMap;

public abstract class LogSubject {

    LogSubject() {

    }

    LogSubject(Object[] args) {

    }

    public abstract HashMap<String, Object> state(); //TODO вместо этого можно юзнуть гсон
}
