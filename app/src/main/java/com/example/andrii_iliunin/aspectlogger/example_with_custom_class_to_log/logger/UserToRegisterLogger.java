package com.example.andrii_iliunin.aspectlogger.example_with_custom_class_to_log.logger;

import com.example.andrii_iliunin.aspectlogger.example_with_custom_class_to_log.entity.UserToRegister;

public class UserToRegisterLogger extends UserLogger<UserToRegister> {

    @Override
    public UserToRegister convert(String methodName, Object[] args) {
        return new UserToRegister(args);
    }

    @Override
    public String describe() {
        return "UserToRegister logger instance";
    }
}
