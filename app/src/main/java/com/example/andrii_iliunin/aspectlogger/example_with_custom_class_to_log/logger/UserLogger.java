package com.example.andrii_iliunin.aspectlogger.example_with_custom_class_to_log.logger;

import com.example.andrii_iliunin.aspectlogger.example_with_custom_class_to_log.entity.LogSubject;
import com.example.andrii_iliunin.aspectlogger.logger.AspectLogger;

import java.util.Map;

abstract class UserLogger<T extends LogSubject> implements AspectLogger<T> {

    @Override
    public void log(T instance) {
        System.out.printf("%n%s:%n", describe());
        for (Map.Entry<String, Object> value : instance.state().entrySet()) {
            System.out.println(String.format("%s: %s", value.getKey(), value.getValue()));
        }
    }

}
