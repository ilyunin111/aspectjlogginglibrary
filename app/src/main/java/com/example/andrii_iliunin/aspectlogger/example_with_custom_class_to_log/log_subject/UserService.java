package com.example.andrii_iliunin.aspectlogger.example_with_custom_class_to_log.log_subject;

import com.example.andrii_iliunin.aspectlogger.annotation.EventLog;

public class UserService {

    public static final String LOG_KEY_USER_TO_REGISTER = "user to register";
    public static final String LOG_KEY_EXISTING_USER = "existing user";

    @EventLog(LOG_KEY_USER_TO_REGISTER)
    public void register(String userName, String password) {
        System.out.println(String.format("Registered! Hello, %s with password %s (oops lol)", userName, password));
    }

    @EventLog(LOG_KEY_EXISTING_USER)
    public void doSmth(String userName, String surname, String telephone) {
        System.out.println(String.format("Done: %s, %s, %s", userName, surname, telephone));
    }

}
