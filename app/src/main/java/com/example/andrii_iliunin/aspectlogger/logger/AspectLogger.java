package com.example.andrii_iliunin.aspectlogger.logger;

public interface AspectLogger<T> {

    void log(T instance);

    T convert(String methodName, Object[] args);

    String describe();

}
