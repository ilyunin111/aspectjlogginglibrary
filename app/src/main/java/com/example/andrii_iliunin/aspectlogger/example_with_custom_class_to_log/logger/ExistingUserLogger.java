package com.example.andrii_iliunin.aspectlogger.example_with_custom_class_to_log.logger;

import com.example.andrii_iliunin.aspectlogger.example_with_custom_class_to_log.entity.ExistingUser;

public class ExistingUserLogger extends UserLogger<ExistingUser> {

    @Override
    public ExistingUser convert(String methodName, Object[] args) {
        return new ExistingUser(args);
    }

    @Override
    public String describe() {
        return "Existing user logger";
    }
}
