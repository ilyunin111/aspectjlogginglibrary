package com.example.andrii_iliunin.aspectlogger;

import com.example.andrii_iliunin.aspectlogger.annotation.EventLog;
import com.example.andrii_iliunin.aspectlogger.factory.AspectLoggerServiceLocator;
import com.example.andrii_iliunin.aspectlogger.logger.AspectLogger;

public class MethodNameLoggingExample {

    private static final String LOG_METHOD_NAME = "default";

    @EventLog(LOG_METHOD_NAME)
    private static boolean methodToBeLogged(String arg) {
        return arg.equals(LOG_METHOD_NAME);
    }

    public static void main(String[] args) {
        AspectLoggerServiceLocator.putLogger(LOG_METHOD_NAME, new MethodNameLogger());
        if (!methodToBeLogged("arg")) {
            methodToBeLogged("twiceArg");
        }
    }

    private static class MethodNameLogger implements AspectLogger<String> {

        @Override
        public void log(String instance) {
            System.out.println(String.format("%s: %s", describe(), instance));
        }

        @Override
        public String convert(String methodName, Object[] args) {
            return methodName;
        }

        @Override
        public String describe() {
            return "Method name logger";
        }
    }

}
