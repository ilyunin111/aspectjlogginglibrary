package com.example.andrii_iliunin.aspectlogger.example_with_custom_class_to_log.entity;

import java.util.HashMap;

public class ExistingUser extends LogSubject {

    private static final String KEY_NAME = "name";
    private static final String KEY_SURNAME = "surname";
    private static final String KEY_TELEPHONE = "telephone";

    private String name;
    private String surname;
    private String telephone;

    public ExistingUser(Object[] args) {
        super(args);
        int index = -1;
        name = (String) args[++index];
        surname = (String) args[++index];
        telephone = (String) args[++index];
    }

    public ExistingUser(String name, String surname, String telephone) {
        this.name = name;
        this.surname = surname;
        this.telephone = telephone;
    }

    @Override
    public HashMap<String, Object> state() {
        HashMap<String, Object> result = new HashMap<>();
        result.put(KEY_NAME, name);
        result.put(KEY_SURNAME, surname);
        result.put(KEY_TELEPHONE, telephone);
        return result;
    }
}
