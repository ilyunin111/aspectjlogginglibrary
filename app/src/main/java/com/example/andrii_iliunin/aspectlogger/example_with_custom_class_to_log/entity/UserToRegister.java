package com.example.andrii_iliunin.aspectlogger.example_with_custom_class_to_log.entity;

import java.util.HashMap;

public class UserToRegister extends LogSubject {

    private static final String KEY_NAME = "name";
    private static final String KEY_PASSWORD = "password";

    private String name;
    private String password;

    public UserToRegister(Object[] args) {
        super(args);
        int index = -1;
        name = (String) args[++index];
        password = (String) args[++index];
    }

    public UserToRegister(String name, String password) {
        this.name = name;
        this.password = password;
    }

    @Override
    public HashMap<String, Object> state() {
        HashMap<String, Object> result = new HashMap<>();
        result.put(KEY_NAME, name);
        result.put(KEY_PASSWORD, password);
        return result;
    }
}
