package com.example.andrii_iliunin.aspectlogger.aspect;

import com.example.andrii_iliunin.aspectlogger.annotation.EventLog;
import com.example.andrii_iliunin.aspectlogger.factory.AspectLoggerServiceLocator;
import com.example.andrii_iliunin.aspectlogger.logger.AspectLogger;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.reflect.MethodSignature;

import java.lang.reflect.Method;

@Aspect
public class AspectWrapper {

    @Around("execution(@com.example.andrii_iliunin.aspectlogger.annotation.EventLog * *(..))")
    public Object weaveJoinPoint(ProceedingJoinPoint joinPoint) throws Throwable {
        MethodSignature methodSignature = (MethodSignature) joinPoint.getSignature();
        Method method = methodSignature.getMethod();
        EventLog annotation = method.getAnnotation(EventLog.class);
        String key = annotation.value();

        AspectLogger targetLogger = AspectLoggerServiceLocator.getLogger(key);

        if (targetLogger != null) {
            targetLogger.log(targetLogger.convert(method.getName(), joinPoint.getArgs()));
        }

        return joinPoint.proceed();
    }


}
