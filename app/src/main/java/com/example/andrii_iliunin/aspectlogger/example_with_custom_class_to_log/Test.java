package com.example.andrii_iliunin.aspectlogger.example_with_custom_class_to_log;

import com.example.andrii_iliunin.aspectlogger.example_with_custom_class_to_log.log_subject.UserService;
import com.example.andrii_iliunin.aspectlogger.example_with_custom_class_to_log.logger.ExistingUserLogger;
import com.example.andrii_iliunin.aspectlogger.example_with_custom_class_to_log.logger.UserToRegisterLogger;
import com.example.andrii_iliunin.aspectlogger.factory.AspectLoggerServiceLocator;

public class Test {

    public static void main(String[] args) {
        AspectLoggerServiceLocator.putLogger(UserService.LOG_KEY_USER_TO_REGISTER, new UserToRegisterLogger());
        AspectLoggerServiceLocator.putLogger(UserService.LOG_KEY_EXISTING_USER, new ExistingUserLogger());
        UserService service = new UserService();
        service.register("user", "P@ssw0rd");
        service.register("user2", "P@ssw0rd");
        service.doSmth("user", "sur", "tel");
        service.doSmth("user2", "sur", "tel");
    }

}
