package com.example.andrii_iliunin.aspectlogger.factory;


import com.example.andrii_iliunin.aspectlogger.logger.AspectLogger;

import java.util.HashMap;
import java.util.Map;

public class AspectLoggerServiceLocator {

    private static Map<String, AspectLogger<?>> loggers = new HashMap<>();
    private static AspectLogger<?> defaultLogger;

    public static AspectLogger<?> getLogger(String key) {
        return loggers.get(key);
    }

    public static <T> void putLogger(String key, AspectLogger<T> logger) {
        loggers.put(key, logger);
    }

    public static void clear() {
        loggers.clear();
    }

    public static AspectLogger<?> getDefaultLogger() {
        return defaultLogger;
    }

    public static void setDefaultLogger(AspectLogger<?> defaultLogger) {
        AspectLoggerServiceLocator.defaultLogger = defaultLogger;
    }
}
